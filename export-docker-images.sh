#!/bin/sh

docker image ls -a --format '{{.Repository}}:{{.Tag}}' | sort -k 1 > docker-images.list
