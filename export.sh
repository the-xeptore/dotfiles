#!/usr/bin/env bash

echo 'exporting explicitly installed packages list...'
pacman -Qeq > pkglist.txt
echo 'exported and saved into pkglist.txt'

echo 'exporting optional dependency packages...'
comm -13 <(pacman -Qqdt | sort) <(pacman -Qqdtt | sort) > optdeplist.txt
echo 'exported and saved into optdeplist.txt'
