export XDG_CONFIG_HOME="$HOME/.config"
export ZDOTDIR="$HOME/.config/zsh"
export ZSH_CACHE_DIR="$ZDOTDIR/cache"

export DEVENV="$HOME/dev/env"
export RUSTUP_HOME="$DEVENV/rustup"
export CARGO_HOME="$DEVENV/cargo"
export CONDA_ROOT="$DEVENV/conda"

export EDITOR="nvim"
export TERMINAL="alacritty"
export BROWSER="firefox"

export GOPATH="$HOME/dev/go"
export GOBIN="$GOPATH/bin"

export PATH="$PATH:$HOME/.bin"
export PATH="$PATH:$CARGO_HOME/bin"
export PATH="$PATH:$HOME/.local/scripts"
export PATH="$PATH:$DEVENV/npm/bin"

# Flutter-web-required environment variable
export CHROME_EXECUTABLE=/usr/bin/google-chrome-stable

