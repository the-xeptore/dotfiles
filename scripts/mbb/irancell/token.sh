curl --connect-timeout 2 --retry 2 --retry-delay 1 -s 'http://192.168.8.1/api/webserver/token' \
	-H 'Connection: keep-alive' \
	-H 'Pragma: no-cache' \
	-H 'Cache-Control: no-cache' \
	-H 'Accept: */*' \
	-H 'X-Requested-With: XMLHttpRequest' \
	-H '__RequestVerificationToken: ' \
	-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36' \
	-H 'Referer: http://192.168.8.1/html/home.html' \
	-H 'Accept-Language: en-US,en;q=0.9' \
	--compressed \
	--insecure | grep -Po '<token>\d+</token>' | grep -Po '\d+'
