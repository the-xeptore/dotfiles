set number
syntax on
colorscheme desert
set tabstop=2
set autoindent
set expandtab
set softtabstop=2
