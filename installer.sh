#!/usr/bin/env bash

pacman -Syu --needed $(comm -12 <(pacman -Slq | sort) <(sort pkglist.txt));

pacman -Syu --needed --asdeps ./optdeplist.txt;

yay -Syu --needed $(comm -13 <(pacman -Slq | sort) <(sort pkglist.txt));
